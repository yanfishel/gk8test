
import Transactions          from './Components/Transactions';

import './styles/App.css';

function App() {

  return (
    <Transactions />
  );
}

export default App;

import axios from 'axios';
import {API} from '../constants/api';

export default class EscanService {

  static async getTransactions({address}){

    const data = {
      ...API.INIT_DATA,
      address
    }

    const query = Object.keys(data).map( (name) => `${name}=${data[name]}` ).join('&');
    const url = `${API.URL}?${query}&apikey=${API.API_KEY}`

    const response = await axios.get(url);

    return response.data.result;
  }

}
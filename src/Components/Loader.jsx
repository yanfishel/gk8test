import React from 'react';
import classes from '../styles/Transactions.module.scss';

const Loader = () => {
  return (
    <div className={classes.loader}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Loader;
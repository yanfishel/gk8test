import React, { useState } from 'react';
import EscanService                 from '../API/EscanService';
import Loader  from './Loader';
import AddressForm                  from './AddressForm';
import TransactionsTable            from './TransactionsTable';

import classes from '../styles/Transactions.module.scss';

const Transactions = () => {

  const [isLoading, setIsLoading] = useState(false);
  const [welcome, setWelcome] = useState(true);
  const [error, setError] = useState(null);
  const [transactions, setTransactions] = useState(null);


  const fetchData = async (address)=>{

    setError(null);

    if(!address){

      setTransactions(null);

      setWelcome(true);

    } else {

      try {
        setIsLoading(true);

        const data = await EscanService.getTransactions({address:address});

        if(Array.isArray(data)){
          setTransactions(data);
        } else {
          setError(data);
        }

      } catch(e) {
        setError(e.message)
      } finally {
        setIsLoading(false)
        setWelcome(false);
      }
    }
  }

  return (
    <div>
      { isLoading && <Loader /> }
      <header className={classes.transactionsHeader}>
        <img height="40" src={"./6-layers-4.svg"} alt="Logo" />
        <AddressForm send={fetchData} />
      </header>
      <div className={classes.contentWrapper}>
        { error && <div className={classes.errorAlert}>{error}</div> }
        { welcome
          ? <h3>Enter the address you want, please ...</h3>
          : <TransactionsTable transactions={transactions} />
        }
      </div>
    </div>
  );
};

export default Transactions;
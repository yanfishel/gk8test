import React, {useState} from 'react';

import classes from '../styles/Transactions.module.scss';

const AddressForm = ({send}) => {

  const [address, setAddress] = useState('');

  const onSend = (e)=>{
    e.preventDefault();
    send(address);
  }

  const reset = e=>{
    e.preventDefault();
    setAddress('')
    send(null);
  }

  return (
    <form className={classes.addressForm}>
      <input type="text" value={address} onChange={(e)=>setAddress(e.target.value)} placeholder="Type an address ..." />
      { address && <button onClick={reset} className={classes.resetBtn}>x</button> }
      <button onClick={onSend}>Get transactions</button>
     </form>
  );
};

export default AddressForm;
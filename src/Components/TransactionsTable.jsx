import React from 'react';

import classes from '../styles/Transactions.module.scss';

const TransactionsTable = ({transactions}) => {

  const Row =({row, idx})=>{
    return(
      <tr key={idx}>
        <td>{ new Date(row.timeStamp*1000).toLocaleString() }</td>
        <td>{ row.from }</td>
        <td>{ row.to }</td>
        <td>{ row.value }</td>
        <td>{ row.confirmations }</td>
        <td>{ row.hash }</td>
      </tr>
    )
  }

  if(transactions === null){
    return null;
  }

  return (
    <table className={classes.transactionsTable}>
      <thead>
        <tr>
          <th>Date & Time</th>
          <th>From</th>
          <th>To</th>
          <th>Value</th>
          <th>Confirmations</th>
          <th>Hash</th>
        </tr>
      </thead>
      <tbody>
        {transactions.length
          ? transactions.map((row, idx)=> <Row row={row} idx={idx} /> )
          : <tr><td colSpan={10}>No transactions found</td></tr>
        }
      </tbody>
    </table>
  );
};

export default TransactionsTable;
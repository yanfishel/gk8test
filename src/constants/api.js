
export const API = {
  URL: 'https://api.etherscan.io/api',
  API_KEY: process.env.REACT_APP_ESCAN_API_KEY,
  INIT_DATA: {
    module: 'account',
    action: 'txlist',
    startblock: 0,
    endblock: 9999,
    address: '',
    offset: '',
    page: '',
    sort: 'asc'
  }
}
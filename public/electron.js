const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');
const path = require('path');
const electron = require('electron');

function createWindow () {

  const screenSize = electron.screen.getPrimaryDisplay().workAreaSize;
    
  const win = new BrowserWindow({
    title: "GK8 Test App",
    width: screenSize.width < 1024 ? screenSize.width : 1024,
    height: screenSize.height < 768 ? screenSize.height : 768,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    }
  })
  win.setMenu(null);
  const startURL = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`;
  win.loadURL(startURL);

  // Open the DevTools.
  //win.webContents.openDevTools()
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {

  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})
